# vuetify-app

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run unit tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run end-to-end tests
```
npm run e2e -- --headless
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
